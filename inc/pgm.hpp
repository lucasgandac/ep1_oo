#ifndef PGM_HPP
#define PGM_HPP
#include <string>
#include <iostream>
#include "imagem.hpp"


using namespace std;

class Pgm : public Imagem { 
 private:
Pgm();
public:
 Pgm(string arquivo);
 ~Pgm();
  void criptografia();

};
#endif
