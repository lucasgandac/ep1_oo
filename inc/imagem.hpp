#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>
#include <iostream>

using namespace std;

class Imagem 
{
private:

	string arquivo;
	string coment;
	string ident;
	int altura;
	int largura;
	int tonalidade;
	string cifra;
	int tamanho;
	int posicao;
	int comeco;
	char * pixel;
public:
	Imagem(string arquivo);
	~Imagem();
	

};
#endif
