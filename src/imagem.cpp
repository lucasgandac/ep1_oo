#include <fstream>
#include <sstream>
#include <string>
#include "imagem.hpp"
#include <iostream>
#include <bits/stdc++.h>
using namespace std;
 
Imagem::Imagem(string arquivo)
{
    ifstream foto; 
    cout << arquivo << endl;
    foto.open(arquivo, ios::binary);
    if (foto.is_open())
    {
        getline (foto, ident);
	getline(foto, coment);
        foto >> coment;
        foto >> tamanho;
	foto >> comeco;
        foto >> cifra;
        foto >> largura;
        foto >> altura;
        foto >> tonalidade;

	int lim = 1;
	int cabecalho = 10;

	while(comeco>=lim)
	{
	lim*= 10;
	cabecalho++;
	}
	while(tamanho>=lim)
	{
	lim*=10;
	cabecalho++;
	}
	while(altura>=lim)
	{
	lim*=10;
	cabecalho++;
	}
	while(largura>=lim)
	{
	lim*=10;
	cabecalho++;
	}
	while(tonalidade>=lim)
	{
	lim*=10;
	cabecalho++;
	}
	cabecalho = cabecalho + cifra.size();
	foto.seekg(cabecalho,ios::end);
        posicao = foto.tellg();
        foto.seekg(cabecalho	, ios::beg);

        pixel = new char [posicao];

        foto.read( pixel,posicao);
        foto.close();
	delete [] pixel;
    }

    else cout << "nao foi possivel abrir a imagem\n";
}
Imagem::~Imagem(){}
